import './App.css';

function App() {
  let variable = {
    sexo: "",
    man: "Hola amigo",
    woman: "Hola amiga",
    other: "Hola Amig@"
  }

  let message = null 
  
  if (variable.sexo === 'man') {
    message = variable.man
  } else if (variable.sexo === 'woman') {
    message = variable.woman
  } else {
    message = variable.other
  }

  return (
    <>
      <h1>{message}</h1>
      <img src="https://facebook.github.io/react/img/logo.svg"/>
      <br/>
      <button onClick={()=>alert('Hello World')}>Hello!!</button>
    </>
  );
}

export default App;
